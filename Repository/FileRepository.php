<?php

namespace lst\MediaBundle\Repository;

use Doctrine\ORM\EntityManager;
use lst\MediaBundle\Entity\File;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method File|null find($id, $lockMode = null, $lockVersion = null)
 * @method File|null findOneBy(array $criteria, array $orderBy = null)
 * @method File[]    findAll()
 * @method File[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileRepository extends ServiceEntityRepository
{
    /** @var EntityManager */
    private $em;
    
    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, File::class);
    }

    public function update(File $file)
    {
        $this->em->persist($file);
        $this->em->flush();
    }
//    public function create(array $data) : File
//    {
//        $file = new File();
//        $this->em->persist($file);
//        $this->em->flush();
//
//        return $file;
//    }
    
    public function delete(File $file) : void
    {
        $this->em->remove($file);
        $this->em->flush();
    }
}
