<?php

namespace lst\MediaBundle\Repository;

use lst\MediaBundle\Entity\Gallery;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Gallery|null find($id, $lockMode = null, $lockVersion = null)
 * @method Gallery|null findOneBy(array $criteria, array $orderBy = null)
 * @method Gallery[]    findAll()
 * @method Gallery[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GalleryRepository extends ServiceEntityRepository
{
    /** @var \Doctrine\ORM\EntityManager */
    private $em;
    
    public function __construct(RegistryInterface $registry)
    {
        $this->em = $registry->getEntityManager();

        parent::__construct($registry, Gallery::class);
    }
    
    public function persist(Gallery $gallery) : Gallery
    {
        $entry = $this->em->merge($gallery);
        $this->em->flush();

        return $entry;
    }
    
    public function delete(Gallery $gallery) : void
    {
        $this->em->remove($gallery);
        $this->em->flush();
    }

    public function refresh(Gallery $gallery)
    {
        $this->em->refresh($gallery);
    }
}
