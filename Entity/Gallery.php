<?php

declare(strict_types=1);

namespace lst\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Gallery Entity
 * @ORM\Table(name="media_galleries")
 * @ORM\Entity(repositoryClass="lst\MediaBundle\Repository\GalleryRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Gallery extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, ExternalId;

    /** @var int */
    protected const ENTITY_TYPE_ID = 10;
    /** @var string */
    public const SINGLE_KEY = 'gallery';
    /** @var string */
    public const MULTIPLE_KEY = 'galleries';
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic", "all"})
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"basic", "all"})
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"basic", "all"})
     */
    protected $subtitle = '';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"basic", "all"})
     */
    protected $description = '';

    /**
     * @Assert\Valid()
     * @ORM\ManyToMany(targetEntity="lst\MediaBundle\Entity\File")
     * @ORM\JoinTable(name="media_galleries_files",
     *      joinColumns={
     *          @ORM\JoinColumn(name="gallery_id", referencedColumnName="id")
     *  },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     *      }
     * )
     * @Groups({"files", "all"})
     */
    protected $files;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->files = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return void
     */
    public function setId(int $id) : void 
    {
        $this->id = $id;
    }
    
    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return void $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubtitle() : string
    {
        return $this->subtitle;
    }

    /**
     * @param string $subtitle
     * @return void
     */
    public function setSubtitle(string $subtitle) : void
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getDescription() : string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return void
     */
    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    /**
     * @return ArrayCollection
     */
    public function getFiles()
    {
        return $this->files;
    }
}
