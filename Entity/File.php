<?php

declare(strict_types=1);

namespace lst\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use lst\CoreBundle\Abstractions\AbstractEntity;
use lst\CoreBundle\Abstractions\Traits\ExternalId;
use lst\CoreBundle\Abstractions\Traits\Timestampable;
use lst\CoreBundle\Interfaces\EntityTypeInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * File Entity
 * @ORM\Table(name="media_files")
 * @ORM\Entity(repositoryClass="lst\MediaBundle\Repository\FileRepository")
 */
class File extends AbstractEntity implements EntityTypeInterface
{
    use Timestampable, ExternalId;

    /** @var int */
    protected const ENTITY_TYPE_ID = 9;
    /** @var string */
    public const SINGLE_KEY = 'file';
    /** @var string */
    public const MULTIPLE_KEY = 'files';

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"basic", "all"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false, options={"default":""})
     * @Groups({"basic", "all"})
     */
    private $title = '';

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false, options={"default":""})
     * @Groups({"basic", "all"})
     */
    private $path = '';

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false, options={"default":""})
     * @Groups({"basic", "all"})
     */
    private $type = '';

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default":"0"})
     * @Groups({"basic", "all"})
     */
    private $size = 0;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false, options={"default":""})
     * @Groups({"basic", "all"})
     */
    private $fileName = '';

    /**
     * @ORM\Column(type="json", options={"default":"{}"})
     * @Groups({"basic", "all"})
     */
    private $extra = [];

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false, options={"default":""})
     * @Groups({"basic", "all"})
     */
    private $alt = '';

    /**
     * @return int
     */
    public function getId() : ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title) : void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path) : void
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type) : void
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getSize() : int
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize(int $size) : void
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return array
     */
    public function getExtra() : array
    {
        return $this->extra;
    }

    /**
     * @param array $extra
     */
    public function setExtra(array $extra) : void
    {
        $this->extra = $extra;
    }

    /**
     * @return string
     */
    public function getAlt(): string
    {
        return $this->alt;
    }

    /**
     * @param string $alt
     */
    public function setAlt(string $alt): void
    {
        $this->alt = $alt;
    }
}
