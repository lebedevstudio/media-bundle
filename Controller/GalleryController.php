<?php

declare(strict_types=1);

namespace lst\MediaBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use lst\MediaBundle\Repository\FileRepository;
use lst\MediaBundle\Entity\File;
use lst\MediaBundle\Entity\Gallery;
use lst\MediaBundle\Repository\GalleryRepository;
use lst\MediaBundle\Exception\FileDuplicateInGalleryException;
use lst\MediaBundle\Exception\FileNotFoundInGalleryException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class GalleryController extends AbstractController
{
    /** @var GalleryRepository */
    private $galleryRepository;
    /** @var Operations */
    protected $operations;
    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(EntityManagerInterface $em, Operations $operations, NormalizerInterface $normalizer, RequestStack $request)
    {
        $this->galleryRepository = $em->getRepository(Gallery::class);
        $this->em = $em;
        $this->operations = $operations;

        parent::__construct($normalizer, $request);
    }

    /**
     * @Route("/media/galleries", name="media.gallery.list", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function getGalleries() : JsonResponse
    {
        return $this->list(Gallery::class, Gallery::MULTIPLE_KEY);
    }

    /**
     * @Route("/media/galleries/{id}", name="media.gallery.get", methods={"GET"}, requirements={"id"="\d+"})
     *
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getGallery(Gallery $gallery) : JsonResponse
    {
        return new JsonResponse([
            Gallery::SINGLE_KEY => $this->normalizer->normalize($gallery, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route("/media/galleries", name="media.gallery.create", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function createGallery() : JsonResponse
    {
        return $this->persist(Gallery::class, Gallery::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/media/galleries/{id}", name="media.gallery.update", methods={"PUT"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function updateGallery(Gallery $gallery) : JsonResponse
    {
        return $this->persist(Gallery::class, Gallery::SINGLE_KEY, $this->request->getContent(), $this->request->getMethod());
    }

    /**
     * @Route("/media/galleries/{id}", name="media.gallery.delete", methods={"DELETE"}, requirements={"id"="\d+"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Gallery $gallery
     * @return JsonResponse
     */
    public function deleteGallery(Gallery $gallery) : JsonResponse
    {
        return $this->delete($gallery);
    }

    /**
     * @Route("/media/galleries/{gallery}/files/{file}", name="media.gallery.add.file", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Gallery $gallery
     * @param File $file
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function addFileToGallery(Gallery $gallery, File $file) : JsonResponse
    {
        $gallery->getFiles()->add($file);
        try {
            $this->galleryRepository->persist($gallery);
        } catch (\Exception $exception) {
            throw new FileDuplicateInGalleryException($gallery->getId(), $file->getId());
        }

        return new JsonResponse([
            Gallery::SINGLE_KEY => $this->normalizer->normalize($gallery, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }

    /**
     * @Route("/media/galleries/{gallery}/files/{file}", name="media.gallery.delete.file", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @param Gallery $gallery
     * @param File $file
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function deleteFileFromGallery(Gallery $gallery, File $file) : JsonResponse
    {
        if (!$gallery->getFiles()->contains($file)) {
            throw new FileNotFoundInGalleryException($gallery->getId(), $file->getId());
        }
        $gallery->getFiles()->removeElement($file);
        $this->galleryRepository->persist($gallery);
        $this->galleryRepository->refresh($gallery); // hack to prevent array to object conversion

        return new JsonResponse([
            Gallery::SINGLE_KEY => $this->normalizer->normalize($gallery, 'array', [
                'groups' => $this->serializationGroups
            ])
        ], $this->responseStatus);
    }
}
