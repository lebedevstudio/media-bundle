<?php

declare(strict_types=1);

namespace lst\MediaBundle\Controller;

use Intervention\Image\ImageManager;
use lst\CoreBundle\Abstractions\AbstractController;
use lst\CoreBundle\Service\Operations\Operations;
use lst\MediaBundle\Configs\Configs;
use lst\MediaBundle\Entity\File;
use lst\MediaBundle\Repository\FileRepository;
use RuntimeException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class FilesController extends AbstractController
{
    /** @var Operations */
    protected $operations;

    private $fileData = [];

    /** @var UploadedFile $fileFromRequest */
    private $fileFromRequest;

    private $fileRepository;

    public function __construct(Operations $operations, NormalizerInterface $normalizer, RequestStack $request, FileRepository $fileRepository)
    {
        $this->operations = $operations;
        $this->fileRepository = $fileRepository;

        parent::__construct($normalizer, $request);

        $this->fileFromRequest = $this->request->files->get('file', null);
    }

    /**
     * @Route("/media/files", name="media.files.list", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function listFiles() : JsonResponse
    {
        return $this->list(File::class, File::MULTIPLE_KEY);
    }

    /**
     * @Route("/media/files/{id}", name="media.file.get", methods={"GET"}, requirements={"id"="\d+"})
     *
     * @param File $file
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function getFileFromRequest(File $file) : JsonResponse
    {
        return new JsonResponse([
                File::SINGLE_KEY => $this->normalizer->normalize($file),
            ], $this->responseStatus
        );
    }

    /**
     * @Route("/media/files", name="media.file.create", methods={"POST"})
     *
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function createFile() : JsonResponse
    {
        if (!$this->fileFromRequest) {
            throw new RuntimeException('File error. Can not get file.');
        }

        $this->getFileData();

        $fileName = $this->fileData['fileName'];

        $this->saveFile($fileName);

        if ($this->isImage($this->fileData['type'])) {
            $this->makePreview($fileName);
        }

        return $this->persist(File::class, File::SINGLE_KEY, json_encode($this->fileData), $this->request->getMethod());
    }

    /**
     * @Route(
     *     "/media/files/{id}",
     *     name="media.file.update",
     *     methods={"POST"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param File $file
     * @return JsonResponse
     * @throws ExceptionInterface
     */
    public function updateFile(File $file) : JsonResponse
    {
        $fileName = $file->getFileName();

        if ($fileName == null) {
            $fileName = str_replace('/uploads/', '', $file->getPath());
            $file->setFileName($fileName);
        }

        if ($this->fileFromRequest) {

            $extensionOfNewFile = $this->getFileExtension();
            if ($extensionOfNewFile !== $file->getType()) {
                throw new RuntimeException('The type of file you upload does not match the existing one');
            }

            $this->saveFile($fileName);

            if ($this->isImage($extensionOfNewFile)) {
                $this->makePreview($fileName);
                $file->setExtra($this->fileData['extra']);
            }
        }

        if (!($this->request->request->get('title') == '')) {
            $file->setTitle($this->request->request->get('title'));
        }
        if (!($this->request->request->get('alt') == '')) {
            $file->setAlt($this->request->request->get('alt'));
        }

        $this->fileRepository->update($file);

        return new JsonResponse([
                File::SINGLE_KEY => $this->normalizer->normalize($file),
            ], $this->responseStatus
        );
    }

    /**
     * @return array
     */
    private function getFileData() : array
    {
        $this->fileData['title'] = trim($this->request->request->get('title', ''));
        $fileExtension = $this->getFileExtension();
        $fileName = md5($this->fileData['title'] . time());
        $this->fileData['fileName'] = $fileName . '.' . $fileExtension;
        $this->fileData['path'] = Configs::UPLOAD_PUBLIC_PATH . $fileName . '.' . $fileExtension;
        $this->fileData['type'] = $fileExtension;
        $this->fileData['size'] = $this->fileFromRequest->getSize();
        $this->fileData['alt'] = trim($this->request->request->get('alt', ''));

        return $this->fileData;
    }

    private function getFileExtension() : string
    {
        return $this->fileFromRequest->guessExtension() ?? $this->fileFromRequest->getClientOriginalExtension();
    }

    /**
     * @param string $fileName
     */
    private function makePreview(string $fileName) : void
    {
        $previewSize200 = 200;
        $previewSize100 = 100;

        $nameOfPreviewFile200 = $this->createImageThumbnail($fileName, $previewSize200);
        $nameOfPreviewFile100 = $this->createImageThumbnail($fileName, $previewSize100);

        $previewFullName200 = Configs::UPLOAD_PUBLIC_PATH . $nameOfPreviewFile200;
        $previewFullName100 = Configs::UPLOAD_PUBLIC_PATH . $nameOfPreviewFile100;

        $this->fileData['extra'] = [
            'preview' => [
                "{$previewSize200}x{$previewSize200}" => $previewFullName200,
                "{$previewSize100}x{$previewSize100}" => $previewFullName100
            ]
        ];
    }

    /**
     * @param $fileName
     */
    private function saveFile(string $fileName) : void
    {
        $this->fileFromRequest->move(Configs::UPLOAD_PHYSICAL_PATH, $fileName);
    }

    /**
     * @param string $type
     * @return bool
     */
    private function isImage(string $type) : bool
    {
        return in_array($type, ['jpg', 'jpeg', 'png']);
    }

    /**
     * @param string $imageName
     * @param int $previewSize
     * @return string
     */
    private function createImageThumbnail(string $imageName, int $previewSize) : string
    {
        $image = (new ImageManager(['driver' => 'gd']))->make(Configs::UPLOAD_PHYSICAL_PATH . $imageName);
        $image->fit($previewSize,$previewSize);
        $name = $image->filename . "_{$previewSize}x{$previewSize}." . $image->extension;
        $image->save(Configs::UPLOAD_PHYSICAL_PATH . $name);

        return $name;
    }

    /**
     * @Route(
     *     "/media/files/{id}",
     *     name="media.file.delete",
     *     methods={"DELETE"},
     *     requirements={"id"="\d+"}
     * )
     * @IsGranted("ROLE_ADMIN")
     *
     * @param File $file
     * @return JsonResponse
     */
    public function deleteFile(File $file): JsonResponse
    {
        $pathWithoutUploadsFolder = str_replace(Configs::UPLOAD_PUBLIC_PATH, '', Configs::UPLOAD_PHYSICAL_PATH);

        if ($file->getPath()) {

            $nameOfPhysicalPathOfFile = $pathWithoutUploadsFolder . $file->getPath();

            if (file_exists($nameOfPhysicalPathOfFile)) {
                unlink($nameOfPhysicalPathOfFile);
            }
        }

        $extra = $file->getExtra();
        if ($extra) {
            if (array_key_exists('preview', $extra)) {
                foreach ($extra['preview'] as $nameOfPreview) {
                    unlink($pathWithoutUploadsFolder . $nameOfPreview);
                }
            }
        }

        return $this->delete($file);
    }
}