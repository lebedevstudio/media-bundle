<?php

declare(strict_types=1);

namespace lst\MediaBundle\Exception;

class FileNotFoundInGalleryException extends \RuntimeException
{
    public function __construct(int $galleryId, int $fileId)
    {
        parent::__construct(sprintf('File with id %d not found in gallery %d', $fileId, $galleryId), 501, null);
    }
}
