<?php

declare(strict_types=1);

namespace lst\MediaBundle\Exception;

class FileDuplicateInGalleryException extends \RuntimeException
{
    public function __construct(int $galleryId, int $fileId)
    {
        parent::__construct(sprintf('File with id %d already exists in gallery %d', $fileId, $galleryId), 501, null);
    }
}
